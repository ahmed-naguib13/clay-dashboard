# Welcome to My Clay Dashboard

----
## Steps to install the project on your machine
clone the repository

    git clone git@gitlab.com:ahmed-naguib13/clay-dashboard.git

then navigate to the created directory 

    cd clay-dashboard

Then run composer install 

    composer install


Then you need to setup the database.
In mysql, create a datbase and call it "clay".

Then run the following command using your 'mysql credentials' to create the tables and add some dummy data 

    mysql -u root -p clay < app/data/dummy-data.sql


now all you have to do is go to the following url

    localhost/clay-dashboard/web/app_dev.php

'Note that: it can be different depending on the machine setup'



in some cases you will get permission issues :
[Check this to fix it](http://symfony.com/doc/2.6/book/installation.html#book-installation-permissions)

## Demo Link

[Watch the demo](https://www.youtube.com/watch?v=RkKDL82SwZw)


## Project Description

Dashboard enables clay customers to :
- add a list of users
- add a list of doors
- assign users to have access to each door
- track events happening to every door
- lock or unlock a specific door

Project is based on symfony framework in order to help with
different modules such as routing, smarter templates(Twig), doctrine, authentication and authorization


### Separation between layers Concept:

## APIBundle
Provides all functionalities, entities and access to database and role validation

## DashboardBundle
serves as a frontend only which consumes the api

## WHY ?
This achieves a better flexibility to hook up any kind of frontened technology such as angular, react or even consume this api layer in a mobile application


>Unfortunately, There was not enough time to make the whole separation, so not all parts follow this rule yet.