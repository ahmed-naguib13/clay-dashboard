-- MySQL dump 10.13  Distrib 5.6.24, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: clay
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clay_door`
--

DROP TABLE IF EXISTS `clay_door`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clay_door` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clay_door`
--

LOCK TABLES `clay_door` WRITE;
/*!40000 ALTER TABLE `clay_door` DISABLE KEYS */;
INSERT INTO `clay_door` VALUES (1,'Office','b60a3892-5961-11e5-885d-feff819cdc9f','unlocked'),(2,'Hallway','c7a96780-5961-11e5-885d-feff819cdc9f','unlocked'),(3,'Main Room','01a6eba2-b178-43c5-81eb-011011645423','locked');
/*!40000 ALTER TABLE `clay_door` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clay_door_user`
--

DROP TABLE IF EXISTS `clay_door_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clay_door_user` (
  `doorId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`doorId`,`userId`),
  KEY `userId` (`userId`),
  CONSTRAINT `clay_door_user_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `clay_user` (`id`),
  CONSTRAINT `clay_door_user_ibfk_1` FOREIGN KEY (`doorId`) REFERENCES `clay_door` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clay_door_user`
--

LOCK TABLES `clay_door_user` WRITE;
/*!40000 ALTER TABLE `clay_door_user` DISABLE KEYS */;
INSERT INTO `clay_door_user` VALUES (1,1),(3,1),(1,2),(1,4);
/*!40000 ALTER TABLE `clay_door_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clay_user`
--

DROP TABLE IF EXISTS `clay_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clay_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(100) NOT NULL,
  `email` varchar(350) NOT NULL,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `roles` text NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `salt` varchar(300) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clay_user`
--

LOCK TABLES `clay_user` WRITE;
/*!40000 ALTER TABLE `clay_user` DISABLE KEYS */;
INSERT INTO `clay_user` VALUES (1,'ahmed','ahmed@test.com','ahmed','naguib','$2a$12$Cmm6b5c5kGC.fD2dqiWz9OYVv9fHpMJLCRXDxraVKBe.37bomaF72','a:1:{i:0;s:10:\"ROLE_ADMIN\";}',1,'spQTv3qU0xoQD7ivJ/2Ljk',NULL),(2,'user1','asd@s.com','asdsa','asdsad','$2a$12$Cmm6b5c5kGC.fD2dqiWz9OYVv9fHpMJLCRXDxraVKBe.37bomaF72','a:1:{i:0;s:9:\"ROLE_USER\";}',1,'EHBNU8YYuAhtX9gQHaVK9R',1),(4,'new','new@email.com','new','user','$2y$12$tmfbwvg0rwgwsgscss0gweO75QEjX3CrL1Z7WgFT1T4ivesUcDEZK','a:1:{i:0;s:9:\"ROLE_USER\";}',1,'tmfbwvg0rwgwsgscss0gwosc888sssc',1);
/*!40000 ALTER TABLE `clay_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clay_user_door_event`
--

DROP TABLE IF EXISTS `clay_user_door_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clay_user_door_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doorId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `event` varchar(50) NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `doorId` (`doorId`),
  KEY `clay_user_door_event_ibfk_3` (`event`),
  CONSTRAINT `clay_user_door_event_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `clay_user` (`id`),
  CONSTRAINT `clay_user_door_event_ibfk_2` FOREIGN KEY (`doorId`) REFERENCES `clay_door` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clay_user_door_event`
--

LOCK TABLES `clay_user_door_event` WRITE;
/*!40000 ALTER TABLE `clay_user_door_event` DISABLE KEYS */;
INSERT INTO `clay_user_door_event` VALUES (4,1,1,'unlock','2015-09-12 22:14:07'),(5,1,1,'lock','2015-09-12 22:38:45'),(6,1,1,'unlock','2015-09-13 11:26:18'),(7,1,1,'lock','2015-09-13 11:28:43'),(24,1,1,'unlock','2015-09-13 11:49:33'),(25,2,1,'lock','2015-09-13 11:50:40'),(26,2,1,'unlock','2015-09-13 11:51:09'),(27,2,1,'lock','2015-09-13 11:53:10'),(28,1,1,'lock','2015-09-13 14:43:35'),(29,2,1,'unlock','2015-09-13 15:59:11'),(30,1,1,'unlock','2015-09-13 18:03:27'),(31,1,2,'lock','2015-09-13 18:19:07'),(32,1,4,'unlock','2015-09-13 18:27:53');
/*!40000 ALTER TABLE `clay_user_door_event` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-13 18:29:13
