<?php
namespace Clay\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

class UserDoorEventRepository extends EntityRepository
{
    public function getDoorEvents($doorCode)
    {
        $query = $this->createQueryBuilder('u')
            ->innerJoin('u.door', 'd' , 'u.doorId = d.id')
            ->where('d.code = :doorCode')
            ->setParameter('doorCode', $doorCode)
            ->orderBy('u.createdAt', 'DESC')
            ->getQuery();

        $doorEvents = $query->execute();

        return $doorEvents;
    }
}