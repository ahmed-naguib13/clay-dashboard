<?php

namespace Clay\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Accessor;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;


/**
 * UserDoorEvent
 *
 * @ORM\Table(name="clay_user_door_event", indexes={@ORM\Index(name="userId", columns={"userId"}), @ORM\Index(name="doorId", columns={"doorId"})})
 * @ORM\Entity(repositoryClass="Clay\ApiBundle\Repository\UserDoorEventRepository")
 *
 * @ExclusionPolicy("all")
 */
class UserDoorEvent
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime", nullable=false)
     * @Expose
     * @SerializedName("createdAt")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Clay\ApiBundle\Entity\Door
     *
     * @ORM\ManyToOne(targetEntity="Clay\ApiBundle\Entity\Door")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="doorId", referencedColumnName="id")
     * })
     *
     */
    private $door;

    /**
     * @var \Clay\ApiBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Clay\ApiBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="userId", referencedColumnName="id")
     * })
     *
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="event", type="string", length=50, nullable=false)
     * @Expose
     */
    private $event;

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Door
     */
    public function getDoor()
    {
        return $this->door;
    }

    /**
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedat($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param Door $door
     */
    public function setDoor($door)
    {
        $this->door = $door;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @param string
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     *
     * @VirtualProperty
     * @SerializedName("user")
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->getUser() ? $this->getUser()->getUsername() : '';
    }

    /**
     * @VirtualProperty
     * @SerializedName("door")
     *
     * @return string
     */
    public function getDoorName()
    {
        return $this->getDoor() ? $this->getDoor()->getName() : '';
    }
}
