<?php

namespace Clay\ApiBundle\Controller;

class DefaultController extends BaseController
{
    public function indexAction()
    {
        return $this->getJsonResponse(json_encode('Welcome to clay api v1.0'));
    }
}
