<?php

namespace Clay\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BaseController extends Controller
{
    protected $allStatuses;

    protected function getUserDetails($id)
    {
        if (!$this->validateUser($id)) {
            throw new HttpException(401, "You don't have access to this user account");
        }
        // get the user using id
        $user = $this->getDoctrine()->getRepository('ClayApiBundle:User')->findOneById($id);
        if(!$user){
            throw new HttpException(400, "Wrong User Id");
        }

        return $user;
    }

    protected function getDoorDetails($id, $doorCode)
    {
        // check if this id belongs to user or not
        if (!$this->validateUser($id)) {
            throw new HttpException(401, "You don't have access to this user account");
        }
        // get the door usng id
        $door = $this->getDoctrine()->getRepository('ClayApiBundle:Door')->findOneByCode($doorCode);

        if (!$door) {
            throw new HttpException(400,'Wrong door code provided');
        }

        return $door;
    }

    protected function getJsonResponse($jsonText, $statusCode = 200)
    {
        return new Response($jsonText, $statusCode, array('Content-Type', 'application/json'));
    }

    protected function validateUser($id)
    {
        if($this->getUser()->getId() != $id){
            return false;
        }

        return true;
    }

    protected function getStatusConfig()
    {
        if($this->allStatuses){
            return $this->allStatuses;
        }

        $this->allStatuses = $this->container->getParameter('doors_config');
        return $this->allStatuses;
    }

    protected function isValidStatus($status)
    {
        $allStatuses = $this->getStatusConfig();

        return in_array( strtolower($status) , array_keys($allStatuses));
    }

    protected function getCorrespondingEvent($status)
    {
        $allStatuses = $this->getStatusConfig();

        return isset($allStatuses[$status]) ? $allStatuses[$status] : '';
    }
}
