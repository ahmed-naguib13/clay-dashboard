<?php

namespace Clay\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Clay\ApiBundle\Entity\UserDoorEvent;
use Clay\ApiBundle\Entity\Door;

class UserController extends BaseController
{
    /**
     * @param $id : user id used to retrieve the user
     * @return serialized{Clay\ApiBundle\Entity\User}
     */
    public function getUserInfoAction($id)
    {
        $logger = $this->get('logger');
        $code = 200;
        try {
            $user = $this->getUserDetails($id);
            $serializer = $this->get('jms_serializer');
            $data = $serializer->serialize($user, 'json');
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            $data = json_encode(array("message" => $e->getMessage()));
            $code = $e->getStatusCode();
        }

        return $this->getJsonResponse($data, $code);
    }

    /**
     * @param $id
     * @return serialized{collection<Clay\ApiBundle\Entity\Door>}
     */
    public function getUserDoorsAction($id)
    {
        $logger = $this->get('logger');
        $code = 200;
        try {
            $user = $this->getUserDetails($id);

            $serializer = $this->get('jms_serializer');
            $data = $serializer->serialize(array('doors' => $user->getDoors()), 'json');
        } catch (\Exception $e) {
            var_dump($e->getMessage());die;
            $logger->error($e->getMessage());
            $data = json_encode(array("message" => $e->getMessage()));
            $code = $e->getStatusCode();
        }

        return $this->getJsonResponse($data, $code);
    }

    /**
     * @param $id
     * @param $doorId
     * @return serialized{Clay\ApiBundle\Entity\Door}
     */
    public function getUserDoorAction($id, $doorCode)
    {
        $logger = $this->get('logger');
        $code = 200;
        try {
            $door = $this->getDoorDetails($id, $doorCode);
            $serializer = $this->get('jms_serializer');
            $data = $serializer->serialize($door, 'json');

        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            $data = json_encode(array("message" => $e->getMessage()));
            $code = $e->getStatusCode();
        }

        return $this->getJsonResponse($data, $code);
    }

    /**
     * @param $id
     * @param $doorId
     * @return serialized{Clay\ApiBundle\Entity\Door}
     */
    public function updateDoorStatusAction(Request $request, $id, $doorCode)
    {
        $logger = $this->get('logger');
        $code = 202;
        try {
            $em = $this->getDoctrine()->getEntityManager();

            $user = $this->getUserDetails($id);
            $door = $this->getDoorDetails($id, $doorCode);



            $content = $request->getContent();
            $data = json_decode($content);

            if (!isset($data->status) || !$this->isValidStatus($data->status)) {
                throw new HttpException(400, 'wrong door status provided');
            }

            if ($door->getStatus() === $data->status) {
                throw new HttpException(400, 'Door is already ' . $door->getStatus());
            }

            if(!$user->getDoors()->contains($door)){
                // add a door event
                $event = new UserDoorEvent();
                $event->setUser($user);
                $event->setDoor($door);
                $event->setCreatedAt(new \DateTime());
                $event->setEvent('unauthorized ' . $this->getCorrespondingEvent($data->status));
                $em->persist($event);
                $em->flush();
                throw new HttpException(401, 'User does not have access to this door');
            }

            // update door status
            $door->setStatus($data->status);
            $em->persist($door);

            // add a door event
            $event = new UserDoorEvent();
            $event->setUser($user);
            $event->setDoor($door);
            $event->setCreatedAt(new \DateTime());
            $event->setEvent($this->getCorrespondingEvent($data->status));
            $em->persist($event);
            $em->flush();

            $serializer = $this->get('jms_serializer');
            $data = $serializer->serialize($door, 'json');
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            $data = json_encode(array("message" => $e->getMessage()));
            $code = $e->getStatusCode();
        }

        return $this->getJsonResponse($data, $code);
    }

    /**
     * @param $id
     * @param $doorId
     * @return serialized{Clay\ApiBundle\Entity\Door}
     */
    public function getUserDoorEventsAction($id, $doorCode)
    {
        $logger = $this->get('logger');
        $code = 200;
        try {
            // calling get door details to validate user and door data
            $this->getDoorDetails($id, $doorCode);

            $events = $this->getDoctrine()->getRepository('ClayApiBundle:UserDoorEvent')->getDoorEvents($doorCode);

            $serializer = $this->get('jms_serializer');
            $data = $serializer->serialize(array('events' => $events), 'json');

        } catch (\Exception $e) {
            $logger->error($e->getMessage());
            $data = json_encode(array("message" => $e->getMessage()));
            $code = $e->getStatusCode();
        }

        return $this->getJsonResponse($data, $code);
    }
}
