<?php

namespace Clay\DashboardBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DoorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('code', 'text')
            ->add('users', 'entity', array(
                'class' => 'ClayApiBundle:User',
                'property'     => 'username',
                'multiple'     => true,
                'expanded'      => true,
            ))
            ->add('submit', 'submit', array(
              'attr' => array('class' => 'btn btn-primary pull-right'),
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Clay\ApiBundle\Entity\Door'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'clay_user';
    }
}
