<?php

namespace Clay\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ClayDashboardBundle:Default:index.html.twig', array('userId'=> $this->getUser()->getId() ));
    }
}
