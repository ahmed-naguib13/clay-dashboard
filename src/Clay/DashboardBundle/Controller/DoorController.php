<?php

namespace Clay\DashboardBundle\Controller;

use Clay\DashboardBundle\Form\DoorType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Clay\ApiBundle\Entity\Door;
use Symfony\Component\HttpFoundation\Request;


class DoorController extends Controller
{
    public function manageAction($code)
    {
        return $this->render('ClayDashboardBundle:Door:manage.html.twig',
            array(
                'userId' => $this->getUser()->getId(),
                'code' => $code,
                'userName' => $this->getUser()->getUserName()
            )
        );
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $door = new Door();
        $door->setStatus('locked');

        $form = $this->createForm(new DoorType(), $door);

        if($request->getMethod() === 'POST'){
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($door);
                $users = $door->getUsers();
                foreach($users as $user){
                    $user->addDoor($door);
                    $em->persist($user);
                }
                $em->flush();

                return $this->redirect($this->generateUrl('clay_dashboard_homepage'));
            }
        }
        return $this->render('ClayDashboardBundle:Door:add.html.twig',
            array(
                'form' => $form->createView(),
                'door' => $door
            )
        );
    }

    public function editAction(Request $request, $code)
    {
        $em = $this->getDoctrine()->getManager();
        $door = $em->getRepository('ClayApiBundle:Door')->findOneByCode($code);

        $form = $this->createForm(new DoorType(), $door);

        if($request->getMethod() === 'POST'){
            $form->handleRequest($request);
            if ($form->isValid()) {
                $em->persist($door);
                $em->flush();
            }
        }

        return $this->render('ClayDashboardBundle:Door:edit.html.twig',
            array(
                'form' => $form->createView(),
                'door' => $door
            )
        );
    }
}
