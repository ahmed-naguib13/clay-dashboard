<?php

namespace Clay\DashboardBundle\Controller;

use Clay\DashboardBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Clay\ApiBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    public function addAction(Request $request)
    {
        $userId = $this->getUser()->getId();
        // create a task and give it some dummy data for this example
        $user = new User();
        $user->setActive(true);
        $user->setParentId($userId);
        $user->setRoles(array('ROLE_USER'));

        $em = $this->getDoctrine()->getManager();
        $logger = $this->get('logger');
        $form = $this->createForm(new UserType(), $user);

        if($request->getMethod() === 'POST'){
            $form->handleRequest($request);
            if ($form->isValid()) {
                try{
                    // encode password before saving
                    $encoderFactory = $this->get('security.encoder_factory');
                    $encoder = $encoderFactory->getEncoder($user);
                    $salt = $user->getSalt();
                    $encodedPassword = $encoder->encodePassword($user->getPassword(),$salt);
                    // set encoded password into user entity
                    $user->setSalt($salt);
                    $user->setPassword($encodedPassword);
                    $em->persist($user);
                    $em->flush();

                    return $this->redirect($this->generateUrl('clay_dashboard_list_user'));
                }catch(\Exception $e){
                    $logger->error($e->getMessage());
                    throw new \Exception('user could not be created');
                }
            }
        }

        return $this->render('ClayDashboardBundle:User:add.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function listAction()
    {
        $userId = $this->getUser()->getId();

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('ClayApiBundle:User')->findByParentId($userId);

        return $this->render('ClayDashboardBundle:User:list.html.twig', array(
            'users' => $users,
        ));
    }

    public function userStatusAction($id,$status)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('ClayApiBundle:User')->findOneById($id);

        if(!$user){
            throw new \Exception('user not found');
        }

        $status = $status === 'enable' ? 1 : 0;

        $user->setActive($status);
        $em->persist($user);
        $em->flush();

        return $this->redirect($this->generateUrl('clay_dashboard_list_user'));
    }
}
