angular.module('clayApp', [])
    .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    })
    .controller('mainController', function ($scope,$http) {
        $scope.doorsList = [];
        $scope.showLoader = true;
        $http.get(baseUrl + 'users/'+ userId + '/doors').
            then(function(response) {
                // this callback will be called asynchronously
                // when the response is available
                console.log(response.data);
                $scope.doorsList = response.data.doors;
                $scope.showLoader = false;
            }, function(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                $scope.showLoader = false;
            });
    });