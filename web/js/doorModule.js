angular.module('clayApp', ['toggle-switch'])
    .config(function($interpolateProvider){
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    })
    .controller('doorsController', function ($scope,$http) {
        // set common variables
        $scope.baseUrl = baseUrl;
        $scope.userId = userId;
        $scope.doorCode = doorCode;
        $scope.currentUserName = currentUserName;

        // initialize objects
        $scope.door = '';
        $scope.doorIcon = '';
        $scope.showLoader = true;
        $scope.switchStatus = false;

        // get door main data
        $http.get(baseUrl + 'users/'+ userId + '/doors/' + doorCode).
            then(function(response) {
                $scope.door = response.data;
                if($scope.door.status == 'unlocked'){
                    $scope.switchStatus = true;
                }
                $scope.updateStatusLabelClass();
                $scope.showLoader = false;
            }, function() {
                $scope.showLoader = false;
            });

        $scope.doorEvents = [];
        $scope.labelClass = 'label-default';

        $scope.showUserName = function(user,currentUserName){
            if(user === $scope.currentUserName){
                return 'you';
            }

            return user;
        };

        $scope.refreshEvents = function(){
            // get door events
            $http.get($scope.baseUrl + 'users/'+ $scope.userId + '/doors/' + $scope.doorCode + '/events').
                then(function(response) {
                    $scope.doorEvents = response.data.events;
                });
        };

        $scope.updateStatus = function() {

            $scope.showLoader = true;

            var status = '';
            if ($scope.switchStatus) {
                status = 'unlocked';
            } else {
                status = 'locked';
            }

            $http.put(baseUrl + 'users/' + userId + '/doors/' + doorCode, {"status": status}).
                then(function (response) {
                    $scope.door = response.data;
                    $scope.updateStatusLabelClass();
                    $scope.doorEvents = $scope.refreshEvents();
                    $scope.showLoader = false;
                }, function () {
                    $scope.showLoader = false;
                });
        };

        $scope.updateStatusLabelClass = function()
        {
            $scope.labelClass = $scope.door.status === 'unlocked'? 'label-primary':'label-default';
        }

        $scope.displayEvent = function(event){

            if(event.event.indexOf('unauthorized') > -1){
                return event.event + ' attempt by ' + $scope.showUserName(event.user) + ' at ';
            }

            return $scope.showUserName(event.user) + ' '+ event.event + 'ed' +  ' the door at ';
        };

        $scope.refreshEvents();

    });